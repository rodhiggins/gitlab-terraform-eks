variable "region" {
  default     = "ap-southeast-2"
  description = "AWS region"
}

variable "cluster_name" {
  default     = "abs-website-eks"
  description = "EKS Cluster for website deploy"
}

variable "cluster_version" {
  default     = "1.27"
  description = "Kubernetes version"
}

variable "instance_type" {
  default     = "t3.small"
  description = "EKS node instance type"
}

variable "instance_count" {
  default     = 1
  description = "EKS node count"
}

variable "agent_namespace" {
  default     = "gitlab-agent"
  description = "Kubernetes namespace to install the Agent"
}

variable "agent_token" {
  description = "glagent-pmwcs_J7QsjdnNMsfCnnKdx4NUCpyG-GWdzuKZmNLfzxZihcXA"
  sensitive   = true
}

variable "kas_address" {
  description = "wss://kas.gitlab.com"
}
